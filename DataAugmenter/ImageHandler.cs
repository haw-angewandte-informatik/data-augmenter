﻿namespace DataAugmenter;

using ImageMagick;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

internal class ImageHandler
{
    private static readonly string[] SupportedExtensions = {
        ".png",
        ".jpg",
        ".jpeg",
        ".heif",

    };

    private static bool IsSupported(string fileName) => SupportedExtensions.Any(fileName.EndsWith);

    public static List<ImageFile> FindFiles(string path)
    {
        var dict = new Dictionary<string, ImageFile>();
        foreach (var file in Directory.EnumerateFiles(path))
        {
            var imageName = Path.GetFileNameWithoutExtension(file);
            if (!dict.TryGetValue(imageName, out var imageFile))
            {
                imageFile = dict[imageName] = new();
            }

            if (IsSupported(file))
            {
                imageFile.File = file;
            }
            else if (Path.GetExtension(file).ToLower() == ".xml")
            {
                imageFile.LabelFile = file;
            }
        }
        var danglingLabels = dict.Where(kvp => kvp.Value.File == null && kvp.Value.LabelFile != null).Select(kvp => kvp.Key).ToList();
        foreach (var lbl in danglingLabels)
        {
            Console.WriteLine($"Warning: Ignoring label file {lbl} without matching image");
            dict.Remove(lbl);
        }

        return dict.Values.ToList();
    }

    internal static async Task FlipFiles(List<ImageFile> images)
    {
        var tasks = new List<Task>();
        foreach (var image in images)
        {
            tasks.Add(FlopFile(image.File!));
            if(image.LabelFile is not null)
                tasks.Add(FlopLabels(image.LabelFile));
        }
        await Task.WhenAll(tasks);
    }

    private static Task FlopFile(string file)
    {
        Console.WriteLine($"Flipping image file {file}");
        var img = new MagickImage(file);
        img.Flop();
        var flippedFileName = GetAbsoluteFlippedFilename(file);
        return img.WriteAsync(flippedFileName);
    }
    private static async Task FlopLabels(string file)
    {
        Console.WriteLine($"Flipping label file {file}");
        XDocument document;
        await using (var readStream = File.OpenRead(file))
        {
            document = await XDocument.LoadAsync(readStream, LoadOptions.PreserveWhitespace, CancellationToken.None);
        }
        var filename = document.Root!.Element("filename")!.Value;
        var imageName = Path.GetFileNameWithoutExtension(filename);
        var ext = Path.GetExtension(filename);
        document.Root!.Element("filename")!.Value = imageName + "_flipped" + ext;
        var path = document.Root!.Element("path")!.Value;
        
        document.Root!.Element("path")!.Value = Path.Join(Path.GetDirectoryName(path), imageName) + "_flipped" + ext;

        var width = int.Parse(document.Root!.Element("size")!.Element("width")!.Value);

        foreach (var lbl in document.Root!.Elements("object").Select(e => e.Element("bndbox")))
        {
            int xMin = int.Parse(lbl!.Element("xmin")!.Value);
            int xMax = int.Parse(lbl.Element("xmax")!.Value);
            int newMin = width - xMax;
            int newMax = width - xMin;
            lbl.Element("xmin")!.Value = newMin.ToString();
            lbl.Element("xmax")!.Value = newMax.ToString();
        }

        var fileNameFlipped = GetAbsoluteFlippedFilename(file);
        await File.WriteAllTextAsync(fileNameFlipped, document.ToString(SaveOptions.DisableFormatting));
    }

    private static string GetAbsoluteFlippedFilename(string file) => Path.Join(Path.GetDirectoryName(file),
        Path.GetFileNameWithoutExtension(file) + "_flipped" + Path.GetExtension(file));
}
