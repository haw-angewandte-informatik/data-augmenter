﻿namespace DataAugmenter
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    internal class ImageFile
    {
        public ImageFile() { }

        public ImageFile(string file, string labelFile)
        {
            File = file;
            LabelFile = labelFile;
        }

        public string? File { get; set; }
        public string? LabelFile { get; set; }
    }
}
