﻿using DataAugmenter;

if (args.Length == 0)
{
    PrintHelp();
    return;
}

var dir = args.Last();
if (!Directory.Exists(dir))
{
    Console.WriteLine($"Not a valid directory: {dir}");
}

// No command
if(args.Length == 1)
{
    var files = ImageHandler.FindFiles(dir);
    PrintOverview(files);
    return;
}

// At least one command

if (args[0] == "mirrorh")
{
    var files = ImageHandler.FindFiles(dir);
    PrintOverview(files);
    await ImageHandler.FlipFiles(files);
    return;
}

Console.WriteLine("No known commands found.");
PrintHelp();

static void PrintHelp()
{
    Console.WriteLine("Small tool to augment images with PascalVOC labels.");
    Console.WriteLine($"USAGE:{Environment.NewLine}DataAugmenter [command] {{Directory}}");
    Console.WriteLine("Commands:");
    Console.WriteLine("mirrorh: Horizontally mirrors images and labels, keeping gravity in the right direction.");
    Console.WriteLine($"{Environment.NewLine}Without a command, the target directory is scanned and an summary of found files is printed.");
}

static void PrintOverview(IReadOnlyCollection<ImageFile> file)
{
    Console.WriteLine($"{file.Count} images found");
    Console.WriteLine($"{file.Count(f => f.LabelFile != null)} with labels");
    Console.WriteLine($"{file.Count(f => f.LabelFile == null)} without labels");
}